# wsl-pdftoprinter-system
A relatively simple job queue system for sending PDF files to [pdftoprinter](http://www.columbia.edu/~em36/pdftoprinter.html) from a Windows Subsystem for Linux (WSL) instance.<br />
This system maintains a fifo queue with PDF files to consecutively print using pdftoprinter.

## Installation
### ArchWSL
```bash
$ git clone https://gitlab.com/martijn-heil/wsl-pdftoprinter-system-git.git
$ cd wsl-pdftoprinter-system
$ makepkg
$ sudo pacman -U *.pkg.tar*
```

## Usage on WSL systems with pacman and (working!) systemd
After installation you can start (and optionally enable) the systemd unit:
```bash
$ sudo systemctl start pdftoprinter-system
$ sudo systemctl enable pdftoprinter-system # If you want to automatically start it on boot.
```
To schedule a PDF to be printed, do as follows:
```bash
$ cat somefile.pdf | pdftoprinter-system-schedule
```
Or rather, without [useless use of cat](https://en.wikipedia.org/wiki/Cat_(Unix)#Useless_use_of_cat):
```bash
$ < somefile.pdf pdftoprinter-system-schedule
```

## Generic usage on unsupported platforms
Ensure that `/run/pdftoprinter/` exists and is readable and writable by the user executing pdftoprinter. <br />

Then you can start it as following:
```bash
$ pdftoprinter-system
```
It is advisable to run pdftoprinter under it's own user for added security.<br />
If you have systemd, it is advisable to run it as a systemd unit.<br />
You can then schedule a PDF file to be printed using the following method:
```bash
$ cat somefile.pdf | pdftoprinter-system-schedule
```
Or rather, without [useless use of cat](https://en.wikipedia.org/wiki/Cat_(Unix)#Useless_use_of_cat):
```bash
$ < somefile.pdf pdftoprinter-system-schedule
```

## Further notes
Currently, stopping pdftoprinter-system discards the job queue. This means among other things that the job queue will not be retained across system reboots.
